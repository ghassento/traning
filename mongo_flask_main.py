from flask import Flask, jsonify, request
from flask_cors import CORS
import pymongo
from pymongo import MongoClient
import uuid

app = Flask(__name__)

# |======== CORS CONFIG =========|
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

client = MongoClient('mongodb://admin:changeMe@localhost:27017/')
db = client['CUSTOMERS']
customers_db = db.customers


@app.route('/', methods=['GET'])
def hello():
    return "Hello CRUD Flask With Mongodb!!!"


@app.route('/customers', methods=['GET'])
def get_customers_list():
    customers = customers_db.find({}, {"_id":0})
    customers = [c for c in customers]
    return jsonify(dict(data=customers)), 200


@app.route('/customers', methods=['POST'])
def create_new_customer():
    payload = request.get_json()
    cid = str(uuid.uuid4())
    payload.update({"cid": cid})
    customers_db.insert_one(payload).inserted_id
    return jsonify(dict(message="customer added successfully")), 201


@app.route('/customers/<customer_id>', methods=['PUT'])
def edit_existing_customer(customer_id):
    payload = request.get_json()
    customers_db.update_one({"cid": customer_id}, {"$set": payload})
    return jsonify(dict(message="customer edited successfully")), 201


@app.route('/customers/<customer_id>', methods=['DELETE'])
def delete_customer(customer_id):
    customers_db.remove({"cid": customer_id})
    return jsonify(dict(message="customer deleted successfully")), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8081)

from flask import Flask, render_template
from google.cloud import storage

app = Flask(__name__)


@app.route('/', methods=['GET'])
def index():
    return "All services : \n GET /hello_world \n GET /hello_user/<name> \n GET /display_photos"

@app.route('/hello_world', methods=['GET'])
def hello_world():
    name ='World'
    return render_template('hello.html', title='Hello', name=name)

@app.route('/hello_user/<name>', methods=['GET'])
def hello(name):
    return render_template('hello.html', title='Hello', name=name)

@app.route('/display_photos', methods=['GET'])
def display_photos():
    """List all files in GCP bucket."""
    client = storage.Client.from_service_account_json('./creds/gcscreds.json')
    bucket_name = "traning_photos"
    bucket = client.bucket(bucket_name)
    files = bucket.list_blobs()
    url_images_list =[]
    for file in files:
        if file._properties['name'].endswith(('jpeg', 'png', 'jpg', 'gif')):
            url_images_list.append(dict(
                    url="https://storage.googleapis.com/traning_photos/" + file._properties['name'],
                    name=file._properties['name'])
                )
    return render_template('display_images.html', title='display images', images_list=url_images_list)

if __name__ == '__main__':

    app.run(host='0.0.0.0', port=8080, debug=True)
